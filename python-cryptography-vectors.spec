%global pypi_name cryptography-vectors
%global source_name cryptography_vectors
Name:           python-%{pypi_name}
Version:        44.0.0
Release:        1
License:        ASL 2.0 or BSD-3-Clause
Summary:        This module is a test vectors for the cryptography package
URL:            https://pypi.python.org/pypi/cryptography-vectors
Source0:        %{pypi_source cryptography_vectors}
BuildArch:      noarch

%description
This package is a test vectors for the python-cryptography.

%package     -n python3-%{pypi_name}
Summary:        This module is a test vectors for the cryptography package
BuildRequires:  python3-devel python3-setuptools python3-pip python3-flit-core
%{?python_provide:%python_provide python3-cryptography-vectors}

%description -n python3-%{pypi_name}
This package is a test vectors for the python-cryptography.

%prep
%autosetup -n %{source_name}-%{version}
rm -vrf cryptography_vectors.egg-info

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-%{pypi_name}
%defattr(-,root,root)
%license LICENSE LICENSE.APACHE LICENSE.BSD
%{python3_sitelib}/%{source_name}/
%{python3_sitelib}/%{source_name}-%{version}.dist-info/*

%changelog
* Thu Jan 16 2025 Ge Wang <wang__ge@126.com> - 44.0.0-1
- Upgrade package to 44.0.0

* Tue May 7 2024 tenglei <tenglei@kylinos.cn> - 42.0.7-1
- upgrade package to 42.0.7
- Fix build with Rust nightly

* Wed Jan 31 2024 liyanan <liyanan61@h-partners.com> - 42.0.2-1
- Upgrade package to 42.0.2

* Fri May 19 2023 Dongxing Wang <dxwangk@isoftstone.com> - 40.0.2-1
- Upgrade package to 40.0.2

* Tue Jan 31 2023 huangduirong <huangduirong@huawei.com> - 39.0.0-1
- Upgrade package to 39.0.0

* Tue Jul 19 2022 OpenStack_SIG <openstack@openeuler.org> - 36.0.1-1
- Upgrade package to version 36.0.1

* Fri Feb  5 2021 lingsheng <lingsheng@huawei.com> - 3.3.1-1
- Update to 3.3.1

* Thu Oct 22 2020 zhangtao <zhangtao221@huawei.com> - 3.0-2
- delete python2

* Mon Jul 27 2020 dingyue<dingyue5@huawei.com> -3.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade software to v3.0

* Thu Apr 16 2020 chengquan<chengquan3@huawei.com> -2.9-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade software to v2.9

* Fri Feb 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.6.1-1
- Update to 2.6.1

* Mon Nov 25 2019 Ling Yang <lingyang2@huawei.com> - 2.3-2
- Package init
